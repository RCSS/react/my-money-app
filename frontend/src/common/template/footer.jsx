import React from 'react'

export default props => (
    <footer className='main-footer'>
        <strong>
            Conteúdo sob licenciamento <a href="https://br.creativecommons.org/" target='_blank'>Creative Commons</a>
        </strong>
    </footer>
)